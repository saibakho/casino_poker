import json
import time, os
import tkinter as tk
from tkinter import *
from tkinter.ttk import *
from threading import Thread

import auto_play as ap
from auto_play import logging, error, auto_run


class Ctrl:
	def __init__(self):
		self.end_thread = False
		self.auto_thread = Thread(target=auto_run)
		self.auto_thread.start()

	def set_cards(self, hold):
		txt = {
			None: "-",
			True: "O",
			False: "X"
		}
		for i, flag in enumerate(hold):
			cards[i]["text"] = txt[flag]

if __name__ == '__main__':
	root = Tk()
	root.title("GBF Casino Poker")
	root.attributes("-topmost", True)
	root.attributes("-alpha", 0.7)
	root.iconbitmap("icon.ico")
	root.resizable(0, 0)
	
	cards = [None]*5
	cards_panel = Labelframe(root, text="Hold Cards")
	cards_panel.pack(fill=X, padx=5, pady=5)
	for i in range(5):
		cards_panel.grid_columnconfigure(i, weight=1, uniform=True)
		cards[i] = Label(cards_panel, width=2, font=("Consolas", 25), anchor="center", foreground="white", background="black", text="-")
		cards[i].grid(row=0, column=i, sticky="nsew", padx=5, pady=5)
	ap.ctrl = Ctrl()

	def on_closing():
		root.destroy()
		ap.ctrl.end_thread = True
		logging("main_thread closed.")
	def check_thread():
		if ap.ctrl.auto_thread and not ap.ctrl.auto_thread.is_alive():
			on_closing()
		root.after(100, check_thread)
	root.protocol("WM_DELETE_WINDOW", on_closing)
	root.after(100, check_thread)
	root.mainloop()
