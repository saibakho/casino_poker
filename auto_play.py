import json
import time, os
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.keys import Keys

def logging(msg, enable=True):
	if enable:
		print("[INFO]", msg)

def error(msg, enable=True):
	if enable:
		print("[ERROR]", msg)

# another solution: infinitive loop
def wait_for_element(selector, root=None, log=False):
	if root == None:
		root = driver
	while True:
		if ctrl.end_thread:
			exit()
		try:
			element = root.find_element_by_css_selector(selector)
			# if no exception, then break/return
			#print(element.location)
			#print(element.size)
			logging("element [%s] found."%selector, enable=log)
			return element
		except NoSuchElementException:
			error("Not able to find element [%s]."%selector, enable=log)
			time.sleep(1)

def wait_till_displayed(element):
	if element == "start-or-double":
		while True:
			start_btn = wait_for_element(".prt-start")
			if start_btn.is_displayed():
				return "start"
			double_btn = wait_for_element(".prt-yes")
			if double_btn.is_displayed():
				return "double"
	elif element == "start-or-captcha":
		pass	# to-do
	else:
		btn_name = {
			"ok": ".prt-ok",
			"start": ".prt-start",
			"double": ".prt-double-select",
		}
		while True:
			btn = wait_for_element(btn_name[element])
			if btn.is_displayed():
				return "done"

def transfer(hold):
	num_trans = ["A"]+[str(num) for num in range(2, 11)]+["J", "Q", "K"]
	#suit_trans = ["Spades ", "Hearts ", "Diamonds ", "Clubs "]
	suit_trans = ["S", "H", "D", "C"]

	trans = []
	for card in hold:
		suit, number = card.split("_")
		if card != "99_99":
			trans.append(suit_trans[int(suit)-1]+num_trans[int(number)-1])
		else:
			trans.append("Joker")
	return trans

def pick_cards(candi_cards):
	if candi_cards == None:
		return

	# make dict
	stra_array = [0]*13
	num_dict = {"99": []}
	suit_dict = {"99": []}
	for card in candi_cards:
		suit, number = card.split("_")
		num_dict[number] = [card] if number not in num_dict else num_dict[number]+[card]
		suit_dict[suit] = [card] if suit not in suit_dict else suit_dict[suit]+[card]
		if card != "99_99":
			stra_array[int(number)-1] = 1
	#print(num_dict)
	#print(suit_dict)

	flush = []		# make an array for flush,
	straight = []	# one for straight,
	pair_kind = []	# and one for pair_kind
	for key, group in suit_dict.items():
		if len(group) > 3-len(suit_dict["99"]) and key != "99":
			flush += group
	flush += suit_dict["99"]
	for key, group in num_dict.items():
		if len(group) > 1 and key != "99":
			pair_kind += group
	pair_kind += num_dict["99"]

	candi_map = {}
	for i in range(9):
		count = sum(stra_array[i:i+5])
		if count > 3 - len(num_dict["99"]):
			candi_map[count] = i
	if candi_map != {}:
		idx = candi_map[max(candi_map)]
		straight = [num_dict[str(j+1)][0] for j in range(idx, idx+5) if stra_array[j] == 1]+num_dict["99"]
	else:
		straight = num_dict["99"]
	
	#print("flush: ", flush)
	#print("straight: ", straight)
	#print("pair_kind: ", pair_kind)

	# four of a kind
	if len(pair_kind) == 4:
		if len(num_dict[pair_kind[0].split("_")[1]]) == 4:
			return pair_kind
	# 5-flush, 5-straight, full house
	for candi in [flush, straight, pair_kind]:
		if len(candi) == 5:
			return candi
	# three of a kind s(and two pair)
	if len(pair_kind) > 2:# == 3:
		return pair_kind
	# 4-flush
	if set(flush)-set(["99_99"]) != set():
		return flush
	# 4-straight
	if set(straight)-set(["99_99"]) != set():
		return straight
	# one pair, or no pair
	return pair_kind

	#############################################################
	# to-do:													#
	# 1. straight is stil buggy	(see the straight bug below)	#
	# 2. with 1 joker, the order of straight/flush and two pair #
	#############################################################

	# 4-flush: 11/50 = 22 %
	# 4-straight: space x 4/50 => 8 ~ 48 %
	# Note: space examples:
	# 	1 space:  5[]678 => 4/50 = 8 %
	# 	2 spaces: []5678[] => 8/50 = 16 %
	# 	3 spaces: []5[]67[] with 1 Joker => 12/50 = 24 %
	# 	4 spaces: [][]567[][] with 1 Joker => 16/50 = 32 %
	# 	5 spaces: [][]5[]6[][] with 2 Jokers => 20/50 = 40 %
	# 	6 spaces: [][][]56[][][] with 2 Jokers => 24/50 = 48 %

def pick_cards_test():
	# pick cards test
	#candi_cards = ['3_4', '99_99', '99_99', '4_4', '3_2']
	#candi_cards = ['4_13', '4_6', '4_10', '2_6', '4_2']
	#candi_cards = ['4_13', '4_6', '4_10', '2_6', '4_3']
	#candi_cards = ['3_4', '2_6', '3_8', '4_5', '1_2']
	#candi_cards = ['1_10', '99_99', '2_4', '2_6', '3_10']
	#candi_cards = ['1_10', '99_99', '2_4', '2_11', '2_10']
	candi_cards = ['1_7', '99_99', '2_8', '2_9', '3_10']	# straight bug
	print("hold: ", pick_cards(candi_cards))
	exit(1)

ctrl = None
driver = None

# URL MACROs
HOME_URL = "http://game.granbluefantasy.jp/#mypage"
LOGIN_URL = "http://game.granbluefantasy.jp/#authentication"
POKER_URL = "http://game.granbluefantasy.jp/#casino/game/poker/200040"

def auto_run():
	try:
		chrome_options = webdriver.ChromeOptions()
		# remove the "chrome is being controlled..." bar
		#chrome_options.addArguments("disable-infobars")	# old selenium api
		chrome_options.add_experimental_option("excludeSwitches", ['enable-automation'])
		# easiest way to deal with login !!!
		chrome_options.add_argument("user-data-dir=%s/Google/Chrome/User Data"%os.getenv("LOCALAPPDATA"))

		global driver
		driver = webdriver.Chrome(options=chrome_options)

		driver.execute_script("window.open('%s', '_blank')"%HOME_URL)
		driver.switch_to.window(driver.window_handles[-1])
		driver.get(POKER_URL)

		prev_cards = candi_cards = None
		while True:
			while candi_cards == prev_cards or candi_cards == None:
				candi_cards = driver.execute_script("return window.cards_1_Array")
				time.sleep(0.5)
			prev_cards = candi_cards

			hold = pick_cards(candi_cards)
			print("hold: ", transfer(hold), "\n")
			ctrl.set_cards([True if card in hold else False for card in candi_cards])

			wait_till_displayed("ok")
			wait_till_displayed("start")
			ctrl.set_cards([None]*5)

	# update driver except / close chrome first except
	#except ???:
	#	error("env var PATH error")	
	#except WebDriverException:
	#	error("env var PATH error")
	#except InvalidArgumentException:
	#	error("multiple chromes")
	finally:
		if driver:
			driver.quit()
		logging("auto_thread closed.")
		#ctrl.master.master.destroy()
	