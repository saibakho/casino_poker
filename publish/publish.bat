pyinstaller -Fw ../casino_poker.py -i ../icon.ico
::	Note:
::		To make the popup window for chromedriver not showing,
::	add the creationflags arg to the Popen options in service.py file:
::
::	# ~/selenium/webdriver/common/service.py
::	self.process = subprocess.Popen(cmd, env=self.env,
::									close_fds=platform.system() != 'Windows',
::									creationflags=0x08000000,
::									stdout=self.log_file,
::									stderr=self.log_file,
::									stdin=PIPE)
::